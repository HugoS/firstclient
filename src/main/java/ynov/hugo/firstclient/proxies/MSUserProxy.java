package ynov.hugo.firstclient.proxies;

import java.util.List;
import java.util.Optional;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import ynov.hugo.firstclient.model.User;


@FeignClient(name = "ms-gateway")
@RibbonClient(name = "firstms")
public interface MSUserProxy {
	
	@GetMapping(value="firstms/users")
	List<User> getUsers();

	@GetMapping(value="firstms/user/{id}")
	Optional<User> getUser(@PathVariable("id") Long id);
}
